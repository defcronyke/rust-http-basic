// A modified example originally from:
// https://github.com/stephank/hyper-staticfile
//
// This example serves static files from either
// `../static` or `static/`.
//
// First optional arg is path to serve, second
// optional arg is port to serve on.

use futures_util::future;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response};
use hyper_staticfile::Static;
use std::io::Error as IoError;
use std::path::Path;
use std::env;
use std::net::SocketAddr;
use hyper::server::conn::AddrStream;

async fn handle_request<B>(req: Request<B>, static_: Static, remote_addr: SocketAddr) -> Result<Response<Body>, IoError> {
    println!("request: {} -> {}", remote_addr, req.uri());

    static_.clone().serve(req).await
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    let pwd = env::var("PWD").unwrap_or_default();
    let pwd = pwd.as_str();
    let mut path1_pre = Path::new(pwd).to_path_buf();
    path1_pre.pop();
    let path1_pre = path1_pre.to_str().unwrap_or_default();
    let path1_string = format!("{}/static/", path1_pre);
    let path1 = path1_string.as_str();
    
    let path2 = "static/";

    let serve_path = if args.len() < 2 {
        if Path::new(path1).exists() {
            path1
        } else if Path::new(path2).exists() {
            path2
        } else {
            "./"
        }
    } else {
        args[1].as_str()
    };

    println!("serving path: {}", serve_path);

    let static_ = Static::new(Path::new(serve_path));

    let make_service = make_service_fn(|conn: &AddrStream| {
        let remote_addr = conn.remote_addr();
        let static_ = static_.clone();
        future::ok::<_, hyper::Error>(service_fn(move |req| handle_request(req, static_.clone(), remote_addr)))
    });

    let default_port = 8089;
    let port = if args.len() > 2 {
        args[2].parse::<u16>().unwrap_or(default_port)
    } else {
        default_port
    };

    let default_ip = [0, 0, 0, 0];
    let ip = default_ip;

    let addr = (ip, port).into();
    let server = hyper::Server::bind(&addr).serve(make_service);
    eprintln!("static file server running at: http://{}", addr);
    server.await.expect("server failed");
}
